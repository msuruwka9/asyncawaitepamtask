﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayMethodsNamespace
{
    public class IntRandomGenerator : IRandomGenerator
    {
        public int Generate()
        {
            return new Random().Next();
        }
    }
}
