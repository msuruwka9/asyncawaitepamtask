﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayMethodsNamespace
{
    public class ArrayMethods
    {
        private static readonly int arrSize = 10;
        private readonly IRandomGenerator randomGenerator;

        public ArrayMethods(IRandomGenerator generator)
        {
            randomGenerator = generator;
        }

        public async Task<int[]> CreateRandomArrayAsync()
        {
            var arr = new int[10];
            await Task.Factory.StartNew(() =>
            {
                for (int i = 0; i < arrSize; i++)
                {
                    arr[i] = randomGenerator.Generate();
                }
            });

            return arr;
        }

        public Task<int[]> MultiplyByRandomNumberAsync(int[] arr)
        {
            if (arr is null)
            {
                throw new ArgumentNullException(nameof(arr));
            }

            if (arr.Length != 10)
            {
                throw new ArgumentException("Array must be of size 10", nameof(arr));
            }

            return this.MultiplyByRandomNumberInternalAsync(arr);
        }

        public async Task<int[]> MultiplyByRandomNumberInternalAsync(int[] arr)
        {
            var randomNumber = randomGenerator.Generate();

            await Task.Factory.StartNew(() =>
            {
                for (int i = 0; i < arrSize; i++)
                {
                    arr[i] *= randomNumber;
                }
            });

            return arr;
        }

        public static Task<int[]> SortArrayByAscendingAsync(int[] arr)
        {
            if (arr is null)
            {
                throw new ArgumentNullException(nameof(arr));
            }

            if (arr.Length != 10)
            {
                throw new ArgumentException("Array must be of size 10", nameof(arr));
            }

            return SortArrayByAscendingInternalAsync(arr);
        }

        public static async Task<int[]> SortArrayByAscendingInternalAsync(int[] arr)
        {
            await Task.Factory.StartNew(() => Array.Sort(arr));

            return arr;
        }

        public static Task<double> CalculateAverageValueAsync(int[] arr)
        {
            if (arr is null)
            {
                throw new ArgumentNullException(nameof(arr));
            }

            if (arr.Length != 10)
            {
                throw new ArgumentException("Array must be of size 10", nameof(arr));
            }

            return CalculateAverageValueInternalAsync(arr);
        }

        public static async Task<double> CalculateAverageValueInternalAsync(int[] arr)
        {
            double average = 0;
            await Task.Factory.StartNew(() =>
            {
                for (int i = 0; i < arrSize; i++)
                {
                    average += arr[i];
                }
            });

            return average / arrSize;
        }
    }
}
