using Microsoft.VisualStudio.TestPlatform.TestHost;
using Moq;

namespace ArrayMethodsTests
{
    public class ArrayMethodsClassTests
    {
        private static readonly int arrSize = 10;
        private static readonly int randomNumber = 5;
        private static int[] arr = new int[] {2, 4, 5, 1, 3, 7, 8, 9, 10, 6};
        private static ArrayMethods? classToTest;

        public ArrayMethodsClassTests() 
        {
            arr = Enumerable.Range(1, arrSize).ToArray();
            var mockRandomGenerator = new Mock<IRandomGenerator>();
            mockRandomGenerator.Setup(x => x.Generate()).Returns(randomNumber);
            classToTest = new ArrayMethods(mockRandomGenerator.Object);
        }

        [Fact]
        public async Task ArrayMethods_CreateRandomArray_ReturnsIntArrayOfSizeTen()
        {
            var methodArr = await classToTest!.CreateRandomArrayAsync();
            for (int i = 0; i < methodArr.Length; i++)
            {
                Assert.Equal(randomNumber, methodArr[i]);
            }
            Assert.Equal(arrSize, methodArr.Length);
        }

        [Fact]
        public async Task ArrayMethods_MultiplyByRandomNumber_MultipliesByRandomNumber()
        {
            var result = await classToTest!.MultiplyByRandomNumberAsync(arr);
            for (int i = 0; i < arr.Length; i++)
            {
                Assert.Equal((i + 1) * randomNumber, result[i]);
            }
        }

        [Theory]
        [InlineData(null)]
        [InlineData(new int[] { })]
        [InlineData(new int[] { 1 })]
        [InlineData(new int[] { 1, 2, 3, 4, 5 })]
        [InlineData(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 })]
        public async Task ArrayMethods_MultiplyByRandomNumberArrayIsNullOrEmptyOrIncorrectLength_ThrowsException(int[] arr)
        {
            if (arr is null)
            {
                #pragma warning disable CS8604 // Possible null reference argument.
                await Assert.ThrowsAsync<ArgumentNullException>(() => classToTest!.MultiplyByRandomNumberAsync(arr));
                #pragma warning restore CS8604 // Possible null reference argument.
            }
            else
            {
                await Assert.ThrowsAsync<ArgumentException>(() => classToTest!.MultiplyByRandomNumberAsync(arr));
            }
        }

        [Fact]
        public async Task ArrayMethods_SortArrayByAscending_SortedCorrectly()
        {
            var sortedArray = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            var result = await ArrayMethods.SortArrayByAscendingAsync(arr);

            for (int i = 0; i < arr.Length; i++)
            {
                Assert.Equal(sortedArray[i], result[i]);
            }
        }

        [Theory]
        [InlineData(null)]
        [InlineData(new int[] { })]
        [InlineData(new int[] { 1 })]
        [InlineData(new int[] { 1, 2, 3, 4, 5 })]
        [InlineData(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 })]
        public async Task ArrayMethods_SortArrayByAscendingArrayIsNullOrEmptyOrIncorrectLength_ThrowsException(int[] arr)
        {
            if (arr is null)
            {
                #pragma warning disable CS8604 // Possible null reference argument.
                await Assert.ThrowsAsync<ArgumentNullException>(() => ArrayMethods.SortArrayByAscendingAsync(arr));
                #pragma warning restore CS8604 // Possible null reference argument.
            }
            else
            {
                await Assert.ThrowsAsync<ArgumentException>(() => ArrayMethods.SortArrayByAscendingAsync(arr));
            }
        }

        [Fact]
        public async Task ArrayMethods_CalculateAverageValue_CalculateCorrectAvarage()
        {
            double avarage = arr.Sum() / (double)arr.Length;
            double resultAverage = await ArrayMethods.CalculateAverageValueAsync(arr);

            Assert.Equal(avarage, resultAverage);
        }

        [Theory]
        [InlineData(null)]
        [InlineData(new int[] { })]
        [InlineData(new int[] { 1 })]
        [InlineData(new int[] { 1, 2, 3, 4, 5 })]
        [InlineData(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 })]
        public async Task ArrayMethods_CalculateAverageValueArrayIsNullOrEmptyOrIncorrectLength_ThrowsException(int[] arr)
        {
            if (arr is null)
            {
                #pragma warning disable CS8604 // Possible null reference argument.
                await Assert.ThrowsAsync<ArgumentNullException>(() => ArrayMethods.CalculateAverageValueAsync(arr));
                #pragma warning restore CS8604 // Possible null reference argument.
            }
            else
            {
                await Assert.ThrowsAsync<ArgumentException>(() => ArrayMethods.CalculateAverageValueAsync(arr));
            }
        }
    }
}