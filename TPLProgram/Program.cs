﻿using ArrayMethodsNamespace;

namespace AsyncAwait
{
#pragma warning disable S1118 // Utility classes should not have public constructors
    internal class Program
#pragma warning restore S1118 // Utility classes should not have public constructors
    {
        static async Task Main()
        {
            var intRandomGenerator = new IntRandomGenerator();
            var arrayMethods = new ArrayMethods(intRandomGenerator);

            var arr = await arrayMethods.CreateRandomArrayAsync();
            Console.Write("Randomly created ");
            PrintArray(arr);

            arr = await arrayMethods.MultiplyByRandomNumberAsync(arr);
            Console.Write("Multiplied by random number ");
            PrintArray(arr);

            arr = await ArrayMethods.SortArrayByAscendingAsync(arr);
            Console.Write("Sorted by ascending ");
            PrintArray(arr);

            var average = await ArrayMethods.CalculateAverageValueAsync(arr);
            Console.WriteLine($"Average is: {average}");
            Console.ReadLine();
        }
        static void PrintArray(int[] arr)
        {
            Console.WriteLine("array:");
            foreach (var i in arr)
            {
                Console.WriteLine(i);
            }
            Console.WriteLine("----------");
        }
    }
}